import {useState } from "react";
import Sidebar from "./components/sidebar";
import Contacts from "./pages/contacts/contacts";
import Login from "./pages/login/login";
import Sizes from "./pages/sizes/sizes";
import Axios from "axios";
import axios from "axios";
import { Route, BrowserRouter, Routes, Navigate } from "react-router-dom"
import Logout from "./pages/logout/logout";

const App = () => {
    const [collapse, setCollapse] = useState(false);
    const [authenticated, setAuthenticated] = useState(false);
    const [modalShow, setModalShow] = useState(false);
    const [isUpdating, setIsUpdating] = useState(false);

    const toggleHandle = () => {
        setCollapse(!collapse);
    }
    const handleAdd = () => {
        setIsUpdating(false);
        setModalShow(true);
    }
    const handleEdit = () => {
        setIsUpdating(true);
        setModalShow(true);
    }
    Axios.interceptors.request.use(
        async config => {
            const userInfo = JSON.parse(localStorage.getItem('userInfo'))
            if (userInfo) {
                const token = userInfo.token;
                config.headers["authorization"] = `Bearer ${token}`;
            }
            return config;
        },
        error => {
            console.log(error)
            Promise.reject(error)
        });
    axios.interceptors.response.use(async (response) => {
        return response
    }, async function (error) {
        const originalRequest = error.config;
        if (error.response.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true;
            localStorage.removeItem('userInfo');
            setAuthenticated(false);
        }
        if (error.response.status === 408) {
            localStorage.removeItem('userInfo');
        }
        return Promise.reject(error);
    });
    return (
        <>

            <BrowserRouter>
                <div className="outer-container d-flex">
                    {authenticated && <Sidebar collapse={collapse} setCollapse={collapse} toggleHandle={toggleHandle} />}
                    <Routes>
                        <Route path="/login" element={<Login setAuthenticated={setAuthenticated} />}>
                        </Route>
                        <Route path="/" element={!authenticated ? <Navigate to='/login' replace /> : <Navigate to='/contacts' replace />}>
                        </Route>
                        <Route path="/contacts" element={!authenticated ? <Navigate to='/login' replace /> :
                            <Contacts

                                handleEdit={handleEdit}
                                handleAdd={handleAdd}
                                modalShow={modalShow}
                                collapse={collapse}
                                setModalShow={setModalShow}
                                isUpdating={isUpdating}
                                toggleHandle={toggleHandle}
                            />} />

                        <Route path="/sizes" element={!authenticated ? <Navigate to='/login' replace /> : <Sizes
                            handleEdit={handleEdit}
                            handleAdd={handleAdd}
                            collapse={collapse}
                            toggleHandle={toggleHandle}
                        />}>

                        </Route>
                        <Route path="/logout" element={!authenticated ? <Navigate to='/login' replace /> : <Logout collapse={collapse}
                            setAuthenticated={setAuthenticated}
                            toggleHandle={toggleHandle}
                        />}>
                        </Route>
                    </Routes>
                </div>
            </BrowserRouter>
        </>
    )
}

export default App;
