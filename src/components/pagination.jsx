import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';

const Pagination = (props) => {
    // Here we use item offsets; we could also use page offsets
    // following the API or data you're working with.
    const [itemOffset, setItemOffset] = useState(0);

    useEffect(() => {
        // Fetch items from another resources.
        const endOffset = itemOffset + props.itemsPerPage;
        console.log(`Loading items from ${itemOffset} to ${endOffset}`);
        props.setCurrentItems(props.data.slice(itemOffset, endOffset));
        props.setPageCount(Math.ceil(props.data.length / props.itemsPerPage));
    }, [itemOffset, props.itemsPerPage, props.data]);

    // Invoke when user click to request another page.
    const handlePageClick = (event) => {
        const newOffset = (event.selected * props.itemsPerPage) % props.data.length;
        console.log(
            `User requested page number ${event.selected}, which is offset ${newOffset}`
        );
        setItemOffset(newOffset);
    };

    return (
        <>
                <span className=" pagination">
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel="Next"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={3}
                        pageCount={props.pageCount}
                        previousLabel="prev"
                        renderOnZeroPageCount={null}

                        className="paginate d-flex"
                        pageClassName="page"
                        activeClassName="active"
                        previousLinkClassName="previous"
                        nextLinkClassName="next"
                        disabledClass="disabled"
                    />
                </span>
        </>
    )
}
export default Pagination;
