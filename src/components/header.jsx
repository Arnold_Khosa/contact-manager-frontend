import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBars,faTimes } from '@fortawesome/free-solid-svg-icons';

const Header = (props) => {
const {toggleHandle,collapse}=props;
const userInfo = JSON.parse(localStorage.getItem('userInfo'));
var user=userInfo.data;
    return (
        <>
            <header className="topbar d-flex">
                <div className="icon-container d-flex btn-for-small">
                    <button className="toggle-btn toggle-button-small" onClick={toggleHandle}> <i>< FontAwesomeIcon icon={!collapse ? faBars : faTimes} /></i></button>
                </div>
                <h6 className="pt-3" >Role:{user.role} | User:{user.firstname+ " "+user.lastname}</h6>
            </header>
        </>
    )
}
export default Header;