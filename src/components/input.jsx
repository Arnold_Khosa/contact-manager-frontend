const Input = (props) => {
   
    return (
        <>
            <div className={props.classWrapper}>
                <label className="form-label">
                    {props.label}
                </label>
                <input type={props.type}
                    className="form-control"
                    id={props.id}
                    placeholder={props.placeholder}
                    max={props.max}
                    min={props.min}
                    pattern={props.pattern}
                    name={props.name}
                    value={props.value}
                    defaultValue={props.defaultValue}
                   required={props.required?true:false}
                />
            </div>
        </>
    )
}
export default Input;