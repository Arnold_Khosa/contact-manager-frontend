import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt, faBars,  faUsers, faList, faTimes } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from "react-router-dom";

const Sidebar = (props) => {
    const { collapse, toggleHandle } = props;
    const userInfo = JSON.parse(localStorage.getItem('userInfo'));
    var user = userInfo.data;
    return (
        <>
            <div className={`side ${!collapse ? 'side-collapse' : ''}`}>
                <div className="icon-container d-flex">
                    <button className="toggle-btn" onClick={toggleHandle}> <i>< FontAwesomeIcon icon={!collapse ? faBars : faTimes} /></i></button>
                </div>
                {/* <div className="icon-container d-flex profile-picture">
                    <img className="profile" src={pic} width={`${!collapse ? "60px" : "250px"}`} height={`${!collapse ? "60px" : "120px"}`} />
                </div> */}
                <div className="navigation">
                    <ul>
                        <li>
                            <NavLink className="navLink" to="/contacts">
                            <i>< FontAwesomeIcon icon={faUsers} /></i>
                            <span className={`link-label ${!collapse ? "collapse" : ''}`}>Contacts</span>
                        </NavLink>
                         </li>
                         {user.role==="administrator" && (<>
                        <li>
                            <NavLink className="navLink" to="/sizes">
                           <i>< FontAwesomeIcon icon={faList} /></i>
                           <span className={`link-label ${!collapse ? "collapse" : ''}`}>T-shirt Sizes</span>
                        </NavLink> 
                        </li>
                        </>
                        )}
                        <li>
                            <NavLink className="navLink" to="/logout">
                           <i>< FontAwesomeIcon icon={faSignOutAlt} /></i>
                           <span className={`link-label ${!collapse ? "collapse" : ''}`}>Sign out</span>
                        </NavLink>
                         </li>
                    </ul>
                </div>
            </div>
        </>
    )
}
export default Sidebar;