import { useEffect } from "react"
import Header from "../../components/header"
import { Navigate } from "react-router-dom";
import Axios from "axios";

const Logout =(props)=>{
    const { collapse, toggleHandle } = props;
   
    useEffect(()=>{
        Axios.post("http://localhost:8000/api/logout", {}).then(res => {
            console.log(res.data.message);
            localStorage.removeItem('userInfo');
            props.setAuthenticated(false);
           
        }).catch(err => {
            console.log(err);
        });
    },[])
    return (
        <>
       <div className={`main column ${!collapse ? 'main-max' : ''}`}>
                <Header toggleHandle={toggleHandle} collapse={collapse} />
                <h4>We are logging you out...</h4>
            </div>
       
        </>
    )
}
export default Logout;