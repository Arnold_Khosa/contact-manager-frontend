import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
import { useState, useEffect } from "react";
import Axios from "axios";
import Header from '../../components/header';
const Sizes = (props) => {
    const { collapse, toggleHandle } = props;
    const [data, setData] = useState([]);
    const [size, setSize] = useState("");
    const [updating, setUpdating] = useState(false);
    const [tempId, setTempId] = useState(0);
   
    const handleDelete = (id) => {
        Axios.delete(`http://localhost:8000/api/size/${id}`).then(res => {
            console.log(res.data.message)
        }).catch(err => {
            console.log(err);
        }).finally(() => {
            fetchData();
        })
    }
    const handleEdit = (id) => {
        setTempId(id);
        setUpdating(true);
        const obj = data.find(x => x.id === id);
        setSize(obj.size);
    }
    const handleUpdate = (id) => {
        Axios.put(`http://localhost:8000/api/size/${id}`, { size: size }).then(res => {
            console.log(res.data.message)
        }).catch(err => {
            console.log(err);
        }).finally(() => {
            fetchData();
            setSize("");
            setUpdating(false);
        })
    }
    const handleAdd = () => {
        if (size === "") {
            prompt("The t-shirt size field is empty");
        } else {
            Axios.post("http://localhost:8000/api/size/add", { size: size }).then(res => {
                console.log(res.data.message)
            }).catch(err => {
                console.log(err);
            }).finally(() => {
                fetchData();
                setSize("");
            });
        }
    }

    const fetchData = () => {
        Axios.get("http://localhost:8000/api/sizes").then(res => {
            setData(res.data.data);
        }).catch(err => {
            console.log(err)
        });
    }
    useEffect(() => { fetchData(); }, [])

    return (
        <>
            <div className={`main column ${!collapse ? 'main-max' : ''}`}>
                <Header toggleHandle={toggleHandle} collapse={collapse} />
                <div className="main-content w-100 row">
                     <div className="d-flex flex-column col-lg-6 col-sm-12" style={{Height:"100px"}}>
                       {!updating ? (
                            <> 
                            <button className="add-btn mt-0" style={{marginLeft:'0px'}} onClick={() => { handleAdd() }}>
                               Add T-shirt Size
                            </button>
                            <br />
                            </> 
                        ) : (
                           <> 
                            <button className="add-btn " style={{marginLeft:'0px'}} onClick={() => { handleUpdate(tempId) }}>
                                Update T-shirt Size
                            </button>
                            <br />
                            </>
                        )}
                        <div className=" ">
                            <input type="text" className="form-control" name="size" value={size} placeholder="Enter S,M,L or XL" onChange={(e) => { setSize(e.target.value); }} />
                        </div>
                        <br />
                        {updating && (<button className="add-btn" style={{marginLeft:'0px'}} onClick={() => {setSize(""); setUpdating(false); }}>
                               Cancel
                            </button>
                            )}
                    </div>
                    <table className="col-lg-6 col-sm-12 border table-sm" style={{height:'183px'}}>
                        <thead >
                            <th>#</th>
                            <th>size</th>
                            <th className="action-label">Actions</th>
                        </thead>
                        <tbody >
                            {data.map((item, key) => (
                                <tr key={key}>
                                    <td>{key + 1}</td>
                                    <td>{item.size}</td>
                                    <td className="action">
                                        <button onClick={() => { handleEdit(item.id) }} className="">
                                            < FontAwesomeIcon icon={faEdit} />
                                        </button>
                                        <button onClick={() => { handleDelete(item.id) }} className="">
                                            < FontAwesomeIcon icon={faTrashAlt} />
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>

                </div>

            </div>
        </>
    )
}
export default Sizes;