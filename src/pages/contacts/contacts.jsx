import Header from "../../components/header";
import ContactForm from "./contactform";
import Axios from "axios"
import Table from "./table";
import { useEffect, useState } from "react";

const Contacts = (props) => {
    const { collapse, handleAdd, handleEdit, modalShow, setModalShow, isUpdating, toggleHandle } = props;
    const [data, setData] = useState([]);
    const [sizes, setSizes] = useState([]);
    const [currentItems,setCurrentItems]=useState([]);
    const [message, setMessage] = useState("");
    const [id, setId] = useState(0);
    const [query,setQuery]= useState("");

    const fetchData = () => {
        Axios.get("http://localhost:8000/api/contacts").then(res => {
            setData(res.data.data);
            setMessage(res.data.message);
        }).catch(err => {
            console.log(err)
        }).finally(()=>{
            setTimeout(()=>{setMessage("")},10000);
        })
    }
    const onSearch=(query)=>{
        if(query===""){
            fetchData();
        }else{
            Axios.post("http://localhost:8000/api/search",{query:query}).then(res => {
                setData(res.data.data);
        }).catch(err => {
            console.log(err)
        });
        }
    }
    const getSizes = () => {
        Axios.get("http://localhost:8000/api/sizes").then(res => {
            setSizes(res.data.data);
        }).catch(err => {
            console.log(err)
        });
    }
    useEffect(() => { fetchData(); getSizes();}, [])
    return (
        <>
            <div className={`main column ${!collapse ? 'main-max' : ''}`}>
                <Header toggleHandle={toggleHandle} collapse={collapse} />
                <div className="d-flex">
                    <button className="add-btn" onClick={handleAdd}> Add contact</button>
                    <div className="space-between">
                       {message!=="" && <div className="alert alert-success text-center pt-1 pb-2 mt-1">
                       {message}
                    </div>}
                    </div>
                    <div className="search pt-1">
                        <input type="text" className="form-control" name="size" placeholder="Search here" onChange={(e) => { onSearch(e.target.value);setQuery(e.target.value);}} />
                    </div>
                </div>
                <div className="main-content column">
                    <Table handleEdit={handleEdit}
                     data={data} 
                     setId={setId}
                     currentItems={currentItems}
                     setCurrentItems={setCurrentItems}
                      fetchData={fetchData} />
                </div>
                <ContactForm
                    isUpdating={isUpdating}
                    setModalShow={setModalShow}
                    show={modalShow}
                    onHide={() => setModalShow(false)}
                    id={id}
                    fetchData={fetchData}
                    setMessage={setMessage}
                    sizes={sizes}
                />
            </div>
        </>
    )
}
export default Contacts;