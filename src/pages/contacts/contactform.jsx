import { Modal, Button } from 'react-bootstrap';
import Input from "../../components/input"
import Axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css"

const ContactForm = (props) => {
    const handleSubmit = (e) => {
        e.preventDefault();

        var formData = {}
        for (var i = 0; i < e.target.length; i++) {
            if (e.target[i].name !== "") {
                const name = e.target[i].name;
                const value = e.target[i].value;
                formData[name] = value;
            }
        }
        if (formData['mobile_number'].length === 10) {
            if (props.isUpdating === false) {
                Axios.post("http://localhost:8000/api/contacts/add", formData).then(res => {
                    console.log(res.data.message)
                }).catch(err => {
                    console.log(err);

                }).finally(() => {
                    props.fetchData();
                    props.onHide();
                })
            }
            else {
                Axios.put(`http://localhost:8000/api/contacts/${props.id}`, formData).then(res => {
                    console.log(res.data.message)
                }).catch(err => {
                    console.log(err);
                }).finally(() => {
                    props.fetchData();
                    props.onHide();
                })
            }
           
        }else {
            prompt("Mobile number must be 10 digits long!")
        }
        if (formData.t_shirt==="null") {
            prompt("Please select t-shirt size!")
        } 
    }
    return (
        <>
            <Modal
                {...props}
                size="md"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.isUpdating === true ? "Update contact details" : "Add new contact"}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <form className="mb-2 me-3" onSubmit={handleSubmit}>
                        <div className="d-flex flex-column">
                            <Input
                                label={"Name"}
                                placeholder={"Name"}
                                name="name"
                                type="text"
                                required
                            />
                            <Input
                                label={"Surname"}
                                placeholder={"Surname"}
                                name="surname"
                                type="text"
                                required
                            />
                            <div className="">
                                <label className="form-label">
                                    Mobile number
                                </label>
                                <input type="text"
                                    className="form-control"
                                    placeholder="eg 0768548872"
                                    max="10"
                                    min="10"
                                    pattern="[0-9]+"
                                    name="mobile_number"
                                    required
                                />
                            </div>
                            <Input
                                label={"Address"}
                                placeholder={"Address"}
                                name="address"
                                type="text"
                                required
                            />
                            <select name="t_shirt" className="form-control">
                                <option selected value="null">---Select T-shirt size--</option>
                                {props.sizes.map((item, key) => (
                                    <option key={key} id={item.size} value={item.size}>{item.size}</option>
                                ))
                                }
                            </select>
                        </div>
                        <Modal.Footer>
                            <Button onClick={props.onHide} type="button" className="btn btn-danger">close </Button>
                            <Button type="submit" style={{ backgroundColor: "#23282D" }} className="btn">Submit</Button>
                        </Modal.Footer>
                    </form>
                </Modal.Body>

            </Modal>
        </>
    )
}
export default ContactForm;