import React from "react"
import { useState } from "react";
import Axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import Pagination from "../../components/pagination";
const Table = (props) => {
    const { handleEdit, data, setId, currentItems,setCurrentItems} = props;
    const [actions, setActions] = useState(true);
    const [pageCount,setPageCount] = useState(0);
    const setDefaults = (data) => {
        //getting names of properties of object
        const names = Object.keys(data)
        names.forEach((name) => {
            var input = document.getElementsByName(name);
            if (input[0]) {
                input[0].defaultValue = data[name]
            }
        })
    }
    const edit = (id) => {
        handleEdit();
        setId(id);
        const obj = data.find(x => x.id === id);
        setTimeout(() => { setDefaults(obj); }, 200);
    }
    const handleDelete = (id) => {
        Axios.delete(`http://localhost:8000/api/contacts/${id}`).then(res => {
            console.log(res.data.message)
        }).catch(err => {
            console.log(err);
        }).finally(()=>{
            props.fetchData();
        })

    }
    const userInfo = JSON.parse(localStorage.getItem('userInfo'));
    var user = userInfo.data;
    return (
        <>
            <table >
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Mobile Number</th>
                    <th>Address</th>
                    <th>T-shirt size</th>
                    {actions && <th className="action-label">Actions</th>}
                </thead>
                <tbody>
                    {currentItems.map((item, key) => (
                        <tr key={key}>
                            <td>{key + 1}</td>
                            <td>{item.name}</td>
                            <td>{item.surname}</td>
                            <td>{item.mobile_number}</td>
                            <td>{item.address}</td>
                            <td>{item.t_shirt}</td>
                            {actions && (
                                <td className="action">
                                    <button onClick={() => { edit(item.id) }} className="">
                                        < FontAwesomeIcon icon={faEdit} />
                                    </button>
                                    {user.role === 'administrator'?(
                                        <button onClick={() => { handleDelete(item.id) }} className="">
                                            < FontAwesomeIcon icon={faTrashAlt} />
                                        </button>
                                    ):null 
                                    }

                                </td>
                            )
                            }

                        </tr>

                    ))
                    }
                </tbody>
            </table>
            
            <Pagination
            itemsPerPage={10}
            data={data}
            setCurrentItems={setCurrentItems}
            currentItems={currentItems}
            setPageCount={setPageCount}
            pageCount={pageCount}
            />

        </>
    )
}
export default Table;