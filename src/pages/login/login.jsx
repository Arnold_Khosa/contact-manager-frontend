import Axios from "axios"
import axios from "axios"
import { useEffect, useState } from "react";
import { useNavigate, Navigate } from 'react-router-dom';


const Login = (props) => {
    const { setAuthenticated, setUser } = props;
    const navigate = useNavigate();
    const [message, setMessage] = useState(false);
    const [email, setEmail] = useState();
    const [password, setPassword] = useState("");

    const login = (e) => {
        e.preventDefault();
        axios.post("http://localhost:8000/api/login", {
            email: email,
            password: password
        }).then(res => {
            localStorage.setItem('userInfo', JSON.stringify(res.data));
            setAuthenticated(true);
            setMessage(false);
            navigate('/contacts');
        }).catch(err => {
            setMessage(true);
        });
    }
    useEffect(() => {
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));
        if (userInfo) {
            if (userInfo["token"]) {
                navigate('/contacts');
                setAuthenticated(true);
            }
        } else {
            console.log('Not Authenticated');
        }
    }, []);
    return (
        <>

            <div className={`main column main-max login`}>
                <form className="m-auto login-form login-form-sm" onSubmit={login}>
                    {message && (
                        <div className="alert alert-danger">
                       Incorrect password or Email
                    </div>
                    )}
                    <div className="form-group">
                        <label htmlFor="email">Email address:</label>
                        <input type="email" className="form-control" name="email" id="email" onChange={(e) => { setEmail(e.target.value); setMessage(false); }} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" name="password" id="pwd" onChange={(e) => { setPassword(e.target.value) }} />
                    </div>

                    <div className="form-group row d-flex justify-content-center">
                        <button type="submit" className="btn btn-primary col-11">Sign in</button>
                    </div>
                </form>
            </div>
        </>
    )
}
export default Login;